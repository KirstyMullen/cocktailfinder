import './App.css';
import { Page } from './stories/Page';

function App() {
  return (
    <Page
      headerText="Cocktail Finder"
      menuItems={[
        {
          text: 'Browse All',
          url: '/all'
        },
        {
          text: 'Random',
          url: '/random'
        }
      ]}
    />
  );
}

export default App;
