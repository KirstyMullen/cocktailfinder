import React from 'react';
import { Header } from './Header';
import './page.css';

export const Page = ({ menuCollapsed, headerText, menuItems }) => (
  <article>
    <Header
      menuCollapsed={menuCollapsed}
      headerText={headerText}
      menuItems={menuItems}
    />

    <section className="container py-3">
      <div className="row">
        <div className="col">
          <h2>Page Title</h2>
          <p>
            Gingerbread macaroon icing bear claw sesame snaps. Chocolate bonbon
            jelly. Chocolate cake sugar plum gingerbread jelly beans halvah pie
            lemon drops pastry. Candy canes fruitcake sesame snaps chocolate bar
            cake cheesecake dragée. Tootsie roll gummi bears sesame snaps tart
            jelly. Oat cake croissant jelly jujubes dragée jelly beans. Sweet
            cotton candy sweet roll croissant carrot cake wafer chocolate cake
            pie. Macaroon muffin ice cream fruitcake tootsie roll cake. Sweet
            roll powder oat cake pudding sweet roll candy. Jelly-o jelly-o
            pastry. Tart candy canes ice cream halvah. Jelly jelly muffin
            powder. Icing wafer cake.
          </p>
          <p>
            Cake bonbon cake gingerbread lemon drops sugar plum wafer croissant
            chocolate cake. Candy bear claw cheesecake biscuit cake donut.
            Dessert chocolate cake cotton candy. Croissant cookie sesame snaps
            cupcake pastry tart macaroon. Gummies sweet carrot cake gummi bears.
            Croissant chocolate cake gingerbread biscuit candy canes bonbon. Ice
            cream gummi bears sugar plum. Cupcake jujubes danish. Bear claw
            sweet chupa chups tart gummies carrot cake jelly-o bear claw tart.
            Carrot cake croissant chocolate bar topping. Tart marshmallow
            gingerbread icing chupa chups cheesecake candy. Danish sweet roll
            oat cake ice cream chocolate bar icing dessert carrot cake.
            Croissant ice cream sweet sesame snaps.
          </p>
        </div>
      </div>
    </section>
  </article>
);
Page.propTypes = {
  ...Header.propTypes
};

Page.defaultProps = {
  menuCollapsed: true,
  menuItems: []
};
