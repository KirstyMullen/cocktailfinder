import React from 'react';

import { Menu } from './Menu';

const MenuItems = [
  {
    text: 'Item 1',
    url: '/item1'
  },
  {
    text: 'Item 2',
    url: '/item2'
  }
];

const MenuModule = {
  title: 'Example/Menu',
  component: Menu,
  parameters: { actions: { argTypesRegex: '^on.*' } }
};

export default MenuModule;

const Template = (args) => <Menu {...args} />;

export const Collapsed = Template.bind({});
Collapsed.args = {
  collapsed: true,
  menuItems: MenuItems,
  menuHeaderText: 'Menu Header Text'
};

Collapsed.parameters = {
  backgrounds: { default: 'dark' }
};

export const Shown = Template.bind({});
Shown.args = {
  collapsed: false,
  menuItems: MenuItems,
  menuHeaderText: 'Menu Header Text'
};
