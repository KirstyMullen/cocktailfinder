import React from 'react';

import { MenuList } from './MenuList';

const MenuListModule = {
  title: 'Example/MenuList',
  component: MenuList
};

export default MenuListModule;

const Template = (args) => <MenuList {...args} />;

export const Empty = Template.bind({});

export const Populated = Template.bind({});

Populated.args = {
  menuItems: [
    {
      text: 'Item 1',
      url: '/item1'
    },
    {
      text: 'Item 2',
      url: '/item2'
    }
  ]
};
