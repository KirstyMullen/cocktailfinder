import React from 'react';
import PropTypes from 'prop-types';

export const MenuList = ({ menuItems }) => (
  <ul className="nav flex-column">
    {menuItems.map((menuItem, index) => (
      <li key={index} className="nav-item">
        <a className="nav-link active" href={menuItem.url}>
          {menuItem.text}
        </a>
      </li>
    ))}
  </ul>
);

MenuList.propTypes = {
  menuItems: PropTypes.arrayOf(PropTypes.shape({}))
};

MenuList.defaultProps = {
  menuItems: []
};
