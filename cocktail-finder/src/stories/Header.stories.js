import React from 'react';
import { Header } from './Header';

const MenuItems = [
  {
    text: 'Item 1',
    url: '/item1'
  },
  {
    text: 'Item 2',
    url: '/item2'
  }
];

const HeaderModule = {
  title: 'Example/Header',
  component: Header
};

export default HeaderModule;

const Template = (args) => <Header {...args} />;

export const MenuCollapsed = Template.bind({});
MenuCollapsed.args = {
  menuCollapsed: true,
  headerText: 'Header Text',
  menuItems: MenuItems
};

export const MenuShown = Template.bind({});
MenuShown.args = {
  menuCollapsed: false,
  headerText: 'Header Text',
  menuItems: MenuItems
};
