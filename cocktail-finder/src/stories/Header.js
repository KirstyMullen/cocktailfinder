import React from 'react';
import PropTypes from 'prop-types';
import './header.scss';
import { Menu } from './Menu';

export const Header = ({ menuCollapsed, headerText, menuItems }) => (
  <header className="navbar navbar-dark sticky-top flex-nowrap">
    <div className="d-flex">
      <div className="menu-button-wrapper">
        <Menu
          collapsed={menuCollapsed}
          menuHeaderText={headerText}
          menuItems={menuItems}
        />
      </div>
      <a className="navbar-brand mx-2" href="/">
        <i className="fas fa-kiwi-bird mr-2"></i>
        <span className="header-text">{headerText}</span>
      </a>
      <nav className="nav header-navigation-items">
        {menuItems.map((menuItem, index) => (
          <a key={index} className="nav-link text-light" href={menuItem.url}>
            {menuItem.text}
          </a>
        ))}
      </nav>
    </div>
    <form className="form-inline">
      <input
        type="search"
        className="form-control"
        placeholder="Search..."
        aria-label="Search"
      />
    </form>
  </header>
);

Header.propTypes = {
  menuCollapsed: PropTypes.bool,
  headerText: PropTypes.string,
  menuItems: Menu.propTypes.menuItems
};

Header.defaultProps = {
  menuCollapsed: true
};
