import React from 'react';
import PropTypes from 'prop-types';
import { MenuList } from './MenuList';
import './menu.scss';
import { breakpointM } from './assets/styles/_breakpoints.scss';

export class Menu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      collapsed: this.props.collapsed,
      WindowSize: window.innerWidth
    };
    this.menuButtonClicked = this.menuButtonClicked.bind(this);
    this.handleResize = this.handleResize.bind(this);
  }
  componentDidMount() {
    window.addEventListener('resize', this.handleResize);
  }
  componentWillUnmount() {
    window.addEventListener('resize', null);
  }
  handleResize() {
    console.log(breakpointM);
    const menuNotShown = window.innerWidth > breakpointM;
    if (menuNotShown) {
      this.setState({
        collapsed: true
      });
    }
  }
  menuButtonClicked() {
    this.setState({
      collapsed: !this.state.collapsed
    });
  }
  render() {
    return (
      <div>
        <button
          type="button"
          className="btn btn-outline-light"
          onClick={this.menuButtonClicked}
        >
          <i className="fas fa-bars"></i>
        </button>
        <div
          aria-expanded={this.state.collapsed ? 'false' : 'true'}
          style={{ display: this.state.collapsed ? 'none' : 'block' }}
          className="menu border-right position-fixed fixed-top fixed-bottom"
        >
          <div className="menu-header d-flex justify-content-between p-2">
            <h3 className="mr-2">{this.props.menuHeaderText}</h3>
            <button
              className="btn btn-secondary"
              onClick={this.menuButtonClicked}
            >
              <i className="fas fa-times"></i>
            </button>
          </div>
          <nav>
            <MenuList menuItems={this.props.menuItems} />
          </nav>
        </div>
      </div>
    );
  }
}

Menu.propTypes = {
  collapsed: PropTypes.bool,
  menuItems: PropTypes.arrayOf(PropTypes.shape({})),
  menuHeaderText: PropTypes.string.isRequired,
  onMenuButtonClick: PropTypes.func
};

Menu.defaultProps = {
  menuItems: [],
  collapsed: true
};
